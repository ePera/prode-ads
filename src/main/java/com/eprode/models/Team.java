package com.eprode.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


public class Team extends Model {

	static {
		validatePresenceOf("idTeam", "name");
	}

	public Team(){

	}

	/**
    *Methods get and set
    **/

	public void setIdTeam(int id){
		set("idTeam",id).saveIt();
	}

	public void setName(String name){
		set("name",name).saveIt();
	}

	public Integer getId(){
		return (int) get("idTeam");
	}

	public String getName(){
		return (String) get("name");
	}
}