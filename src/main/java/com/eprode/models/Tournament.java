package com.eprode.models;

import java.util.*;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;
import spark.Request;
import spark.Response;


public class Tournament extends Model {

  public Tournament(){}

  public Tournament(int idUser, int phase, int points){
    set("idUser", idUser);
    set("phase", phase);
    set("points",  points);
  }

  /**
  *Calculate the user's score in the requested phase 
  *@Param int idUser, int phase
  **/

  public void calculateScore(int idUser, int phase){
    List<Prediction> pred = Prediction.where("user = ? and phase = ?", idUser, phase);

    Tournament s = new Tournament(idUser, phase, 0);
    int aux = 0;

    for (int i = 0; i < pred.size(); i++) {

      int idContest = (Integer)(pred.get(i)).get("contest");
          
      List<Contest> t = Contest.where("idContest = ? and phase = ?", idContest, phase);
      int already = (Integer)t.get(0).get("result");
      int recent = (Integer)(pred.get(i)).get("result");
      
      if(already == recent){
        aux += 1;
      }
    }
    s.set("points", aux);
    s.saveIt();
  }

  public int getPoints()
  {return (int) get("points");}
}