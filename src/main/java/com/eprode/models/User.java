package com.eprode.models;

import static spark.Spark.*;
import spark.ModelAndView;
//import spark.template.mustache.MustacheTemplateEngine;
import spark.Request;
import spark.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import org.javalite.activejdbc.Base;


import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

public class User extends Model {

  	/**
  	*Constructor of the User class
  	**/
  	public User(){}

  	/**
  	*Constructor of the User class
  	*@Param name of user, email= email of user, password= pass of user
  	**/
  	public User(String name, String pass, String email){
  		User u = new User();
  		u.set("userName", name);
  		u.set("password",pass);
  		u.set("email",email);
  		u.saveIt();  
 	 }

 	/**
 	*Register a new user on db
 	*@Param req
 	**/

	public Map registerUser(Request req){
	    String user_name = req.queryParams("userName");
	    String password = req.queryParams("password");
	    String email = req.queryParams("email");

	    String body = req.body();
	    Map questt = new HashMap();

	    List<User> unico = User.where("userName = ?", user_name);
	    Boolean result2 = unico.size()==0;
	    if(result2){
	      //User u = new User(user_name,password,email);
	    	User u = new User();
	    	u.set("userName", user_name);
  			u.set("password",password);
  			u.set("email",email);
  			u.saveIt();
	    }
	    else{
	      User u = unico.get(0);
	      String e = (String)u.get("userName");
	      if(e.equals(user_name)){
	      	questt.put("error","<div class='alert alert-danger' id='alert-danger'><strong>Error!</strong> Ese nombre de usuario ya existe, pruebe con uno diferente.</div>");
	        return questt;
	      }
	    }
	    return questt;
	}

	/**
 	*Check if the user and her password exist on db
 	*@Param req
 	**/

	public Map checkUser(Request req){
  		String user_name = req.queryParams("userName");
  		String password = req.queryParams("password");
		String body = req.body();
		Map questt = new HashMap();

		List<User> unico = User.where("userName = ? and password = ?", user_name, password);
		Boolean resu2 = unico.size()!=0;

		if(resu2){
			questt.put("user", unico.get(0).get("idUser"));
			return questt;
		}
		questt.put("user", 0);
		return questt;
	}
}