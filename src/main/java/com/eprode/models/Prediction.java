package com.eprode.models;

import static spark.Spark.*;
import spark.ModelAndView;
//import spark.template.mustache.MustacheTemplateEngine;
import spark.Request;
import spark.Response;
import java.util.HashMap;
import java.util.Map;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.validation.UniquenessValidator;
import java.util.ArrayList;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


public class Prediction extends Model {

    /**
     *Constructor of the User class
     **/
     public Prediction(){
    }

     /**
     *Constructor of the User class
     *@Param username=name of user, email= email of user, password= pass of user
     **/
     public Prediction(int result, int idUser, int idContest, int phase){
        set("result", result);
        set("user", idUser);
        set("contest", idContest);
        set("phase", phase);
    }

    /**
    *Record the user's pre-conditions on a phase
    *@Param req
    **/

    public void registerPrediction(Request req){

        String[] idContest = {req.queryParams("id1"), req.queryParams("id2"), req.queryParams("id3"), req.queryParams("id4"), req.queryParams("id5"), req.queryParams("id6")};
        String[] result = {req.queryParams("gameOne"), req.queryParams("gameTwo"), req.queryParams("gameThree"), req.queryParams("gameFour"), req.queryParams("gameFive"), req.queryParams("gameSix")};
        int phase = Integer.parseInt(req.queryParams("phase").toString());

        int idUser = (Integer)req.session().attribute("user");

        Prediction p = new Prediction(Integer.parseInt(req.queryParams("gameOne")), idUser, Integer.parseInt(req.queryParams("id1")), phase);
        p.saveIt();
        p = new Prediction(Integer.parseInt(req.queryParams("gameTwo")), idUser, Integer.parseInt(req.queryParams("id2")), phase);
        p.saveIt();
        p = new Prediction(Integer.parseInt(req.queryParams("gameThree")), idUser, Integer.parseInt(req.queryParams("id3")), phase);
        p.saveIt();
        p = new Prediction(Integer.parseInt(req.queryParams("gameFour")), idUser, Integer.parseInt(req.queryParams("id4")), phase);
        p.saveIt();
        p = new Prediction(Integer.parseInt(req.queryParams("gameFive")), idUser, Integer.parseInt(req.queryParams("id5")), phase);
        p.saveIt();
        p = new Prediction(Integer.parseInt(req.queryParams("gameSix")), idUser, Integer.parseInt(req.queryParams("id6")), phase);
        p.saveIt();

        Tournament t = new Tournament();
        t.calculateScore(idUser, phase);
    }
}