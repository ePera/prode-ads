package com.eprode.models;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;


public class Contest extends Model {
    /**
     *Constructor of the User class
     **/
     public Contest(){}

     /**
     *Constructor of the Match class
     *@Param idContest= local match goals, team1= visit match goals, team2= team2 of match, tournament= tournament of match
     **/
     public Contest(int result, int phase){
        set("result",result);
        set("phase", phase);
     	saveIt();
     }

     /**
     *Methods get and set
     **/
     
     public void setResult(int result)
     {set("result",result).saveIt();}
     
     public void setPhase(int phase)
     {set("phase",phase).saveIt();}
     
     public int getIdContest()
     {return (int) get("idContest");}

     public int getResult()
     {return (int) get("result");}
     
     public int getPhase()
     {return (int) get("phase");}
    
}