package com.eprode;


import com.eprode.models.*;
import static spark.Spark.*;
import static spark.Spark.staticFileLocation;
import org.javalite.activejdbc.Base;
import static spark.Spark.*;
import spark.ModelAndView;
import spark.template.mustache.MustacheTemplateEngine;
import java.util.HashMap;
import java.util.Map;

/**
  *Clase Principal que administra el juego y sus subprocesos.
  * @author Guevara, Renzo; Pera, Eduardo.
  * @version 0.1
 */
public class App
{
	 public static void main( String[] args )
	    {
	    	staticFiles.location("/html");

			before((req, res) ->{
    			Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1/eprode_dev?nullNamePatternMatchesAll=true", "root", "root");
        	});

			after((req,res) ->{
        		Base.close();
        	});

			Map map = new HashMap();

			get("/start", (req, res) -> {
					return new ModelAndView(map, "./html/start.html");
           		}, new MustacheTemplateEngine()
        		);
		
		    //HashMap con los valores del perfil del usuario de la sesion iniciada.
			Map map2 = new HashMap();

			//HashMap que almacena la cantidad de predicciones acertadas por cada usuario iniciado.-
    		Map prediccionAcertada = new HashMap();
    

			get("/register", (req, res) -> {
      				return new ModelAndView(map2, "./html/register.html");
      			}, new MustacheTemplateEngine()
      			);

			post("/register", (req, res) -> {       
	   				User newUser = new User();
	    			Map result = newUser.registerUser(req);

	    			if((String)result.get("error") != null){
	      				return new ModelAndView(result,"./html/register.html"); 
	    			}
	    			return new ModelAndView(result, "./html/start.html");
	  			}, new MustacheTemplateEngine()
	  			);

			get("/login", (req, res) -> {
	  				User user = new User();
	  				Map logresul = user.checkUser(req);

	  				if((Integer)logresul.get("user") != 0){
	  					req.session().attribute("user", (Integer)logresul.get("user"));
						return new ModelAndView(logresul, "./html/menu.html");
	  				}

	  				return new ModelAndView(logresul,"./html/start.html");
	  			}, new MustacheTemplateEngine()
	  			);

			Map option1_1 = new HashMap();

			get("/eliminationPhaseA", (req, res) -> {
					return new ModelAndView(option1_1, "./html/eliminationPhaseA.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_2 = new HashMap();

			get("/eliminationPhaseB", (req, res) -> {
					return new ModelAndView(option1_2, "./html/eliminationPhaseB.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_3 = new HashMap();

			get("/eliminationPhaseC", (req, res) -> {
					return new ModelAndView(option1_3, "./html/eliminationPhaseC.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_4 = new HashMap();

			get("/eliminationPhaseD", (req, res) -> {
					return new ModelAndView(option1_4, "./html/eliminationPhaseD.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_5 = new HashMap();

			get("/eliminationPhaseE", (req, res) -> {
					return new ModelAndView(option1_5, "./html/eliminationPhaseE.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_6 = new HashMap();

			get("/eliminationPhaseF", (req, res) -> {
					return new ModelAndView(option1_6, "./html/eliminationPhaseF.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_7 = new HashMap();

			get("/eliminationPhaseG", (req, res) -> {
					return new ModelAndView(option1_7, "./html/eliminationPhaseG.html");
				}, new MustacheTemplateEngine()
				);

			Map option1_8 = new HashMap();

			get("/eliminationPhaseH", (req, res) -> {
					return new ModelAndView(option1_8, "./html/eliminationPhaseH.html");
				}, new MustacheTemplateEngine()
				);

			Map option2 = new HashMap();

			get("/roundOf16", (req, res) -> {
					return new ModelAndView(option2, "./html/roundOf16.html");
				}, new MustacheTemplateEngine()
				);

			Map option3 = new HashMap();

			get("/quartersFinals", (req, res) -> {
					return new ModelAndView(option3, "./html/quartersFinals.html");
				}, new MustacheTemplateEngine()
				);

			Map option4 = new HashMap();

			get("/semifinals", (req, res) -> {
					return new ModelAndView(option4, "./html/semifinals.html");
				}, new MustacheTemplateEngine()
				);

			Map option5 = new HashMap();

			get("/final&3rdPlase", (req, res) -> {
					return new ModelAndView(option5, "./html/final&3rdPlase.html");
				}, new MustacheTemplateEngine()
				);

			Map pred = new HashMap();

			post("/uploadPredictions", (req, res) -> {
					Prediction pre = new Prediction();
					pre.registerPrediction(req);
					return new ModelAndView(pred, "./html/eliminationPhase.html");
				}, new MustacheTemplateEngine()
				);
	}
}
