CREATE TABLE contests (
    idContest INT(11) auto_increment PRIMARY KEY,
    result INT(11),
	phase INT(11),
    created_at DATETIME,
    updated_at DATETIME
) ENGINE=InnoDB;