CREATE TABLE predictions (
	idPrediction INT(11) auto_increment PRIMARY KEY,
	user INT(11),
	contest INT(11),
	phase INT(11),
	result INT(11),
    created_at DATETIME,
    updated_at DATETIME
) ENGINE=InnoDB;