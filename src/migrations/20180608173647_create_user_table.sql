CREATE TABLE users (
	idUser INT(11) auto_increment PRIMARY KEY,
	userName VARCHAR(15),
	email VARCHAR(100),
	password VARCHAR(20),
	role VARCHAR(20),
	points INT(11),
    created_at DATETIME,
    updated_at DATETIME
) ENGINE=InnoDB;