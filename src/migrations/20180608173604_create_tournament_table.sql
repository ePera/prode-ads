CREATE TABLE tournaments (
    idTournament INT(11) auto_increment PRIMARY KEY,
    idUser INT(11),
    phase INT(11),
    points INT(11),
    created_at DATETIME,
    updated_at DATETIME
) ENGINE=InnoDB;