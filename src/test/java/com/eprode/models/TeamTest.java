package com.eprode.models;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.test.DBSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TeamTest extends DBSpec {
	@Before
    public void before(){
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1/prode_test?nullNamePatternMatchesAll=true", "root", "12345678");
        System.out.println("TeamTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("TeamTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }
	
    @Test
    public void shouldValidateRequiredAttributes() {
        Team team = new Team();
        the(team).shouldBe("valid");
    }

}