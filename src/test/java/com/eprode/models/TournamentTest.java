package com.eprode.models;

import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.test.DBSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TournamentTest extends DBSpec {
	@Before
    public void before(){
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1/prode_test?nullNamePatternMatchesAll=true", "root", "12345678");
        System.out.println("TournamentTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("TournamentTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }
	
    @Test
    public void shouldValidateRequiredAttributes() {
        Tournament Tournament = new Tournament();
        the(Tournament).shouldNotBe("valid");
    }

}
