package com.eprode.models;

//import com.eprode.User;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.test.DBSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContestTest extends DBSpec {
	
	@Before
    public void before(){
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1/prode_test?nullNamePatternMatchesAll=true", "root", "12345678");
        System.out.println("ContestTest setup");
        Base.openTransaction();
    }

    @After
    public void after(){
        System.out.println("ContestTest tearDown");
        Base.rollbackTransaction();
        Base.close();
    }

    /*@Test
    public void equiposRivalesNoIguales(){
        boolean equiposIguales = (team1 == team2);
        assertEquals(equiposIguales, false);
    }*/
	
    @Test
    public void shouldValidateRequiredAttributes() {
        Contest Contest = new Contest(0,0,1,0,0);
        the(Contest).shouldNotBe("valid");
    }

}